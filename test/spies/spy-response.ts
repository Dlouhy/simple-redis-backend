import {SimpleResponse} from '../../src/interfaces/http';
import {Response} from 'express';
import {expect} from 'chai';

export class SpyResponse implements SimpleResponse {
    public currentStatusCode: number | undefined;
    public currentJsonData: object | undefined;
    public hasBeenSent = false;

    public status(code: number): Response {
        this.currentStatusCode = code;
        return this as unknown as Response;
    }

    public json(body: object): Response {
        this.hasBeenSent = true;
        this.currentJsonData = body;
        return this as unknown as Response;
    }

    public send(): Response {
        this.hasBeenSent = true;
        return this as unknown as Response;
    }

    public expectStatusCodeWasSent(code: number): void {
        expect(this.hasBeenSent).to.be.true;
        expect(this.currentStatusCode).to.be.equal(code);
    }

    public expectDataWasSent(data: object): void {
        expect(this.hasBeenSent).to.be.true;
        expect(this.currentJsonData).to.be.deep.equal(data);
    }
}
