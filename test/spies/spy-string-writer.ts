import {StringWriter} from '../../src/services/string-writer/string-writer';
import {expect} from 'chai';

export class SpyStringWriter implements StringWriter {
    public appendedStrings: Array<string> = [];

    public async append(data: string): Promise<void> {
        this.appendedStrings.push(data);
    }

    public expectNoDataWasStored(): void {
        expect(this.appendedStrings).to.be.empty;
    }

    public expectDataStored(data: string): void {
        expect(this.appendedStrings).to.contain(data);
    }
}
