import {CountStorage} from '../../src/services/count-storage/count-storage';
import {expect} from 'chai';

export class SpyCountStorage implements CountStorage {
    public increments: Array<number> = [];
    public isConnected = false;

    public async connect(): Promise<void> {
        this.isConnected = true;
    }

    public async getCurrent(): Promise<number> {
        return this.increments.reduce((previousValue, currentValue) => previousValue + currentValue, 0);
    }

    public async increaseCount(count: number): Promise<void> {
        this.increments.push(count);
    }

    public expectNoDataWasStored(): void {
        expect(this.increments).to.be.empty;
    }

    public expectCountWasIncreasedOnceByValue(value: number): void {
        expect(this.increments).to.have.length(1);
        expect(this.increments[0]).to.be.equal(value);
    }
}
