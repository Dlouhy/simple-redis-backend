import {SimpleRequest} from '../../src/interfaces/http';

export class SpyRequest implements SimpleRequest {
    public constructor(public readonly body: unknown) {}
}
