import * as express from 'express';
import * as bodyParser from 'body-parser';
import {initialize} from 'express-openapi';
import {apiDoc} from './api/api-doc';
import {getPathsDefinitions} from './utils/get-paths-definitions';
import {NextFunction, Request, Response} from 'express';
import {AppDependenciesBuilder} from './utils/app-dependencies-builder';
import {EnvConfigProvider} from './config/env-config-provider';

prepareApp().catch((error) => console.error(`Could not start application, due to: ${error.message}`));

async function prepareApp(): Promise<void> {
    const config = new EnvConfigProvider().provide();
    const app = express();
    const dependencies = await new AppDependenciesBuilder(config).build();

    app.use(bodyParser.json());

    initialize({
        apiDoc,
        app,
        dependencies,
        pathsIgnore: new RegExp('.(spec|test)$'),
        ...getPathsDefinitions(config.env),
    });

    // The unused "next" argument has to be there for proper functionality
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    app.use(((err, req: Request, res: Response, next: NextFunction) => {
        res.status(err.status).json(err);
    }) as express.ErrorRequestHandler);

    app.listen(config.appPort, () => console.log('Server has started on port 3000'));
}
