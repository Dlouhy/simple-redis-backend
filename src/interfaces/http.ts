import {Request as ExpressRequest, Response as ExpressResponse} from 'express';

export type SimpleRequest = Pick<ExpressRequest, 'body'>;
export type SimpleResponse = Pick<ExpressResponse, 'status' | 'json' | 'send'>;
