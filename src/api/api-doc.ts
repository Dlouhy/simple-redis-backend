export const apiDoc = {
    swagger: '2.0',
    basePath: '/',
    info: {
        title: 'Simple backend server',
        version: '1.0.0',
    },
    definitions: {},
    paths: {},
};
