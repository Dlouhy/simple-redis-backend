import {Operation} from 'express-openapi';
import {StringWriter} from '../../services/string-writer/string-writer';
import {SimpleRequest, SimpleResponse} from '../../interfaces/http';
import {get, isObjectLike} from 'lodash';
import {StatusCodes} from 'http-status-codes';
import {CountStorage} from '../../services/count-storage/count-storage';

export default function (stringWriter: StringWriter, countStorage: CountStorage): Operation {
    const operations = {POST};

    async function POST(req: SimpleRequest, res: SimpleResponse): Promise<SimpleResponse> {
        if (isObjectLike(req.body) && Object.keys(req.body).length > 0) {
            await handleRequestData(req.body);

            return res.status(StatusCodes.OK).send();
        } else {
            return res.status(StatusCodes.BAD_REQUEST).json({message: 'Request body has to be non-empty object'});
        }
    }

    async function handleRequestData(data: object): Promise<void> {
        const stringifiedData = JSON.stringify(data);

        await stringWriter.append(stringifiedData);

        if (isCountObject(data)) {
            await countStorage.increaseCount(data.count);
        }
    }

    POST.apiDoc = {
        description: 'Track incoming data',
        operationId: 'track',
        responses: {
            200: {
                description: 'Successful response',
            },
            default: {
                description: 'An error occurred',
                schema: {
                    additionalProperties: true,
                },
            },
        },
    };

    return operations;
}

// TODO: Maybe separate this?
function isCountObject(input: unknown): input is {count: number} {
    return typeof get(input, 'count') === 'number';
}
