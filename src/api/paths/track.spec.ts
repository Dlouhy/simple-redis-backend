import track from './track';
import {SpyStringWriter} from '../../../test/spies/spy-string-writer';
import {SimpleRequest, SimpleResponse} from '../../interfaces/http';
import {SpyRequest} from '../../../test/spies/spy-request';
import {SpyResponse} from '../../../test/spies/spy-response';
import {StringWriter} from '../../services/string-writer/string-writer';
import {StatusCodes} from 'http-status-codes';
import {CountStorage} from '../../services/count-storage/count-storage';
import {SpyCountStorage} from '../../../test/spies/spy-count-storage';

type SimpleRequestHandler = (req: SimpleRequest, res: SimpleResponse) => Promise<SimpleResponse>;

describe('Track', () => {
    describe('POST method', () => {
        describe('when called', () => {
            describe('without data', () => {
                it('should return BAD_REQUEST status code and store no data', async () => {
                    await givenData(undefined).expect((writer, storage, response) => {
                        writer.expectNoDataWasStored();
                        storage.expectNoDataWasStored();
                        response.expectStatusCodeWasSent(StatusCodes.BAD_REQUEST);
                        response.expectDataWasSent({message: 'Request body has to be non-empty object'});
                    });
                });
            });

            describe('with non-object data', () => {
                it('should return BAD_REQUEST code and store no data', async () => {
                    await givenData(5).expect((writer, storage, response) => {
                        writer.expectNoDataWasStored();
                        storage.expectNoDataWasStored();
                        response.expectStatusCodeWasSent(StatusCodes.BAD_REQUEST);
                        response.expectDataWasSent({message: 'Request body has to be non-empty object'});
                    });
                });
            });

            describe('with object data', () => {
                it('should return OK status code and store data', async () => {
                    const data = {baf: 'buf'};

                    await givenData(data).expect((writer, storage, response) => {
                        storage.expectNoDataWasStored();
                        writer.expectDataStored(JSON.stringify(data));
                        response.expectStatusCodeWasSent(StatusCodes.OK);
                    });
                });

                describe('and object contains "count" property', () => {
                    describe('which is not number', () => {
                        it('should return OK code and store only data and not count', async () => {
                            const data = {count: 'baf'};

                            await givenData(data).expect((writer, storage, response) => {
                                writer.expectDataStored(JSON.stringify(data));
                                storage.expectNoDataWasStored();
                                response.expectStatusCodeWasSent(StatusCodes.OK);
                            });
                        });
                    });

                    describe('which is number', () => {
                        it('should return OK code and store data and count', async () => {
                            const data = {count: 5};

                            await givenData(data).expect((writer, storage, response) => {
                                writer.expectDataStored(JSON.stringify(data));
                                storage.expectCountWasIncreasedOnceByValue(5);
                                response.expectStatusCodeWasSent(StatusCodes.OK);
                            });
                        });
                    });
                });
            });
        });
    });

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    function givenData(data: unknown) {
        const writer = new SpyStringWriter();
        const storage = new SpyCountStorage();
        const method = getTrackPostMethod(writer, storage);
        const request = new SpyRequest(data);
        const response = new SpyResponse();

        return {
            async expect(expectFunction: (writer: SpyStringWriter, storage: SpyCountStorage, response: SpyResponse) => void): Promise<void> {
                await method(request, response);

                expectFunction(writer, storage, response);
            },
        };
    }

    function getTrackPostMethod(writer: StringWriter, storage: CountStorage): SimpleRequestHandler {
        const operations = track(writer, storage);

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return operations.POST as SimpleRequestHandler;
    }
});
