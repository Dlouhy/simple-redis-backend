import {ExpressOpenAPIArgs} from 'express-openapi';

type PathsDefinitions = Pick<ExpressOpenAPIArgs, 'paths' | 'routesGlob' | 'routesIndexFileRegExp'>;

export function getPathsDefinitions(environment?: string | undefined): PathsDefinitions {
    const basePath = 'src/api/paths';

    if (environment === 'production') {
        return {
            paths: `dist/${basePath}`,
            routesGlob: '**/*.js',
            routesIndexFileRegExp: /(?:index)?\.js$/,
        };
    } else {
        return {
            paths: basePath,
            routesGlob: '**/*.ts',
            routesIndexFileRegExp: /(?:index)?\.ts$/,
        };
    }
}
