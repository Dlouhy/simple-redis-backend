import {expect} from 'chai';
import {getPathsDefinitions} from './get-paths-definitions';

describe('getPathsDefinitions', () => {
    describe('when called', () => {
        describe('without parameter', () => {
            it('should return development definition', () => {
                expect(getPathsDefinitions()).to.be.deep.equal({
                    paths: 'src/api/paths',
                    routesGlob: '**/*.ts',
                    routesIndexFileRegExp: /(?:index)?\.ts$/,
                });
            });
        });

        describe('with parameter "production"', () => {
            it('should return production definition', () => {
                expect(getPathsDefinitions('production')).to.be.deep.equal({
                    paths: 'dist/src/api/paths',
                    routesGlob: '**/*.js',
                    routesIndexFileRegExp: /(?:index)?\.js$/,
                });
            });
        });

        describe('with different parameter than "production"', () => {
            it('should return production definition', () => {
                expect(getPathsDefinitions()).to.be.deep.equal({
                    paths: 'src/api/paths',
                    routesGlob: '**/*.ts',
                    routesIndexFileRegExp: /(?:index)?\.ts$/,
                });
            });
        });
    });
});
