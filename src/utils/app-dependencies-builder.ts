import {StringWriter} from '../services/string-writer/string-writer';
import {FileStringWriter} from '../services/string-writer/file-string-writer';
import {CountStorage} from '../services/count-storage/count-storage';
import {Config} from '../config/config-provider';
import {RedisCountStorage} from '../services/count-storage/redis-count-storage';
import {SpyCountStorage} from '../../test/spies/spy-count-storage';

export interface AppDependencies {
    stringWriter: StringWriter;
    countStorage: CountStorage;
}

export class AppDependenciesBuilder {
    public constructor(private readonly config: Config) {}

    public async build(): Promise<AppDependencies> {
        const countStorage = this.getCountStorage();

        await countStorage.connect();

        return {
            countStorage,
            stringWriter: new FileStringWriter(),
        };
    }

    private getCountStorage(): CountStorage {
        if (this.config.useFakeRedis) {
            return new SpyCountStorage();
        } else {
            return new RedisCountStorage(this.config.redis);
        }
    }
}
