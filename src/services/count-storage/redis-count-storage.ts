import {CountStorage} from './count-storage';
import {createClient, RedisClientType} from 'redis';

export interface RedisConfig {
    host: string;
    port: string;
    countKey: string;
}

export class RedisCountStorage implements CountStorage {
    private readonly redisClient: RedisClientType;

    public constructor(private readonly config: RedisConfig) {
        this.redisClient = createClient({url: `redis://${config.host}:${config.port}`});

        this.redisClient.on('error', (err) => {
            throw new Error(err);
        });
    }

    public async connect(): Promise<void> {
        await this.redisClient.connect();
    }

    public async getCurrent(): Promise<number> {
        const maybeCount = await this.redisClient.get(this.config.countKey);

        if (this.isValidCountValue(maybeCount)) {
            return maybeCount;
        } else {
            return 0;
        }
    }

    public async increaseCount(count: number): Promise<void> {
        await this.redisClient.incrByFloat(this.config.countKey, count);
    }

    private isValidCountValue(maybeCount: string | null | number): maybeCount is number {
        return maybeCount !== null && Number.isFinite(+maybeCount);
    }
}
