export interface CountStorage {
    connect(): Promise<void>;
    increaseCount(count: number): Promise<void>;
    getCurrent(): Promise<number>;
}
