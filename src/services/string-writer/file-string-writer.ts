import {StringWriter} from './string-writer';
import {appendFile} from 'fs/promises';
import {join} from 'path';

export class FileStringWriter implements StringWriter {
    public async append(data: string): Promise<void> {
        await appendFile(join(process.cwd(), './data/accepted-data.txt'), data, {encoding: 'utf8'});
    }
}
