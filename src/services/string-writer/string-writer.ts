export interface StringWriter {
    append(data: string): Promise<void>;
}
