import {config} from 'dotenv';
import {Config, ConfigProvider} from './config-provider';
import {RedisConfig} from '../services/count-storage/redis-count-storage';
import {join} from 'path';

export class EnvConfigProvider implements ConfigProvider {
    public constructor() {
        config({path: join(process.cwd(), 'ops/.env'), override: false});
    }

    public provide(): Config {
        const useFakeRedis = this.getEnvVariable('USE_FAKE_REDIS') === 'true';

        return {
            useFakeRedis,
            redis: this.getRedisConfig(useFakeRedis),
            appPort: this.getEnvVariable('APP_PORT'),
            env: this.getEnvVariable('NODE_ENV'),
        };
    }

    private getRedisConfig(useFake: boolean): RedisConfig {
        if (useFake) {
            return {
                host: '',
                port: '',
                countKey: '',
            };
        } else {
            return {
                host: this.getEnvVariable('REDIS_HOST'),
                port: this.getEnvVariable('REDIS_PORT'),
                countKey: 'count',
            };
        }
    }

    private getEnvVariable(name: string): string {
        const maybeValue = process.env[name];

        if (maybeValue !== undefined) {
            return maybeValue;
        } else {
            throw new Error(`Could not find env variable "${name}".`);
        }
    }
}
