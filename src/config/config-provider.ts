import {RedisConfig} from '../services/count-storage/redis-count-storage';

export interface Config {
    useFakeRedis: boolean;
    redis: RedisConfig;
    appPort: string;
    env: string;
}

export interface ConfigProvider {
    provide(): Config;
}
