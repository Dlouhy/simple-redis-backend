# simple-redis-backend

Small test backend

##TODO:
- [ ] Add proper logging and use logger
- [ ] Add /count route
- [ ] Add end-2-end tests
- [ ] Create proper README
- [ ] Better error handling

## Getting Started

These instructions will help you get project up and running.

### Prerequisites

You need to have either node, npm and redis installed for native setup or docker engine and docker compose for running in docker machine.

For docker engine follow this URL: [Get Started With Docker](https://www.docker.com/get-started)

For node, npm and Redis follow these URLs: [Get Started With Node](https://nodejs.org/en/), [Get Started With Redis](https://redislabs.com/resources/get-started-with-redis/)

### Configuration

For local development copy `ops/sample.env` and its contents to `ops/.env`

#### App Config

- APP_PORT - port the system will start listening on.
- NODE_ENV - runtime environment
- USE_FAKE_REDIS - set this variable to `true` to avoid using Redis

#### Redis Config

- REDIS_HOST - Redis host *(default: 127.0.0.1)*
- REDIS_PORT - Redis port *(default: 6379)*

### Installing

#### Native Setup

First install NPM packages using this command

```
npm install
```

Then make sure you have installed and started Redis service.

Run this command:

```
npm start
```

#### Docker Compose

Right now is only available development environment

Run this command:

```
docker-compose -f docker-compose.dev.yml up
```
