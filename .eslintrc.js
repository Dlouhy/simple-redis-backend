module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    parserOptions: {
        project: './tsconfig.json'
    },
    plugins: ['@typescript-eslint', 'mocha', 'import', 'prettier'],
    extends: ['eslint:recommended', 'plugin:@typescript-eslint/eslint-recommended', 'plugin:@typescript-eslint/recommended'],
    rules: {
        'object-shorthand': ['error', 'always'],
        '@typescript-eslint/no-use-before-define': 'off',
        '@typescript-eslint/camelcase': 'off',
        '@typescript-eslint/explicit-function-return-type': 2,
        '@typescript-eslint/unbound-method': ['error'],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        'arrow-parens': ['error', 'always'],
        'mocha/no-exclusive-tests': 'error',
        'import/no-internal-modules': [
            'error',
            {
                allow: ['fp-ts/lib/*'],
            },
        ],
        'prettier/prettier': [
            'error',
            {
                singleQuote: true,
                tabWidth: 4,
                printWidth: 160,
                bracketSpacing: false,
            },
        ],
        'import/no-extraneous-dependencies': ['error', {'devDependencies': true, 'optionalDependencies': false, 'peerDependencies': false}]
    },
};
